package com.practice.kotlin_retrofit_proj.repository

import com.practice.kotlin_retrofit_proj.models.Post
import com.practice.kotlin_retrofit_proj.network.RetroFitBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn


class PostRepository {

    companion object {
        fun getAllPosts(): Flow<List<Post>> = flow {
            val response = RetroFitBuilder.api.getAllPosts()
            emit(response)
        }.flowOn(Dispatchers.IO)


        fun getById(id: Int): Flow<Post> = flow {
            val respon = RetroFitBuilder.api.getDataById(id)
            emit(respon)
        }.flowOn(Dispatchers.IO)
    }
}