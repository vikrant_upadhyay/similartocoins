package com.practice.kotlin_retrofit_proj.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.practice.kotlin_retrofit_proj.adapters.HomeRecyclerViewAdapter
import com.practice.kotlin_retrofit_proj.databinding.FragmentHomeBinding
import com.practice.kotlin_retrofit_proj.viewModels.HomeViewModel

class HomeFragment : Fragment() {
    private val viewModel: HomeViewModel by viewModels()
    lateinit var binding: FragmentHomeBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater)

        binding.lifecycleOwner = this

        binding.viewModel = viewModel

        binding.recyclerViewOnHome.adapter = HomeRecyclerViewAdapter(viewModel)

        return binding.root
    }
}