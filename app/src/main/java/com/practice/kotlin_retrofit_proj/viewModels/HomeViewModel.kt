package com.practice.kotlin_retrofit_proj.viewModels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.practice.kotlin_retrofit_proj.fragments.HomeFragment
import com.practice.kotlin_retrofit_proj.models.Post
import com.practice.kotlin_retrofit_proj.repository.PostRepository
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {
    val dataOfPosts: MutableLiveData<List<Post>> = MutableLiveData()


    init {
        getPost()
    }

    fun getPost() {
        viewModelScope.launch {
            PostRepository.getAllPosts()
                .catch { e -> Log.d("main", "getAllPosts ${e.message}") }
                .collectLatest { response ->
                    println("##RESPONSE ${response}...!!!")
                    dataOfPosts.value = response
                }

        }
    }
}