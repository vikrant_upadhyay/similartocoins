package com.practice.kotlin_retrofit_proj.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.inflate
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.practice.kotlin_retrofit_proj.R
import com.practice.kotlin_retrofit_proj.databinding.ActivityMainBinding.inflate
import com.practice.kotlin_retrofit_proj.databinding.EachRowBinding
import com.practice.kotlin_retrofit_proj.databinding.EachRowBinding.inflate
import com.practice.kotlin_retrofit_proj.models.Post
import com.practice.kotlin_retrofit_proj.repository.PostRepository
import com.practice.kotlin_retrofit_proj.viewModels.HomeViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class HomeRecyclerViewAdapter(viewModel: HomeViewModel) :
    ListAdapter<Post, HomeRecyclerViewAdapter.HomeViewHolder>(Diffcallback) {


    class HomeViewHolder(private var binding: EachRowBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var clickEvent: Boolean = false
        fun bind(post: Post) {
            binding.post = post
            binding.id.text = post.id.toString()
            binding.userId.text = post.userId.toString()
            binding.completed.text = post.completed.toString()
            binding.dropImageResource.setImageResource(R.drawable.ic_baseline_arrow_drop_down_24)
            binding.dropImageResource.setOnClickListener {
                if (clickEvent) {
                    binding.dropImageResource.setImageResource(R.drawable.ic_baseline_arrow_drop_down_24)
                    binding.secondInnerLayout.visibility = View.GONE
                    clickEvent = false
                } else {
                    binding.dropImageResource.setImageResource(R.drawable.ic_baseline_arrow_drop_up_24)
                    binding.secondInnerLayout.visibility = View.VISIBLE
                    clickEvent = true
                }
            }
        }
    }

    companion object Diffcallback : DiffUtil.ItemCallback<Post>() {
        override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
            return oldItem.id == newItem.id
                    && oldItem.userId == newItem.userId
                    && oldItem.title == newItem.title
                    && oldItem.completed == newItem.completed
        }

        override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
            return oldItem.id == newItem.id
                    && oldItem.userId == newItem.userId
                    && oldItem.title == newItem.title
                    && oldItem.completed == newItem.completed
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        return HomeViewHolder(EachRowBinding.inflate(LayoutInflater.from(parent.context)))
    }


    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        val postPojo = getItem(position)
        holder.bind(postPojo)
        holder.itemView.setOnClickListener {

            CoroutineScope(Dispatchers.IO).launch {
                PostRepository.getById(postPojo.id)
                    .catch { e -> Log.d("main", "getAllPosts ${e.message}") }
                    .collectLatest { respons ->
                        println("##RESPONSE ${respons}...!!!")
                    }
            }
        }
    }
}