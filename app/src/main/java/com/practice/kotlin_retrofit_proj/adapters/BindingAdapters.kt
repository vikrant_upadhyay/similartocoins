package com.practice.kotlin_retrofit_proj.adapters

import android.view.View
import android.widget.ProgressBar
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.practice.kotlin_retrofit_proj.models.Post


@BindingAdapter("listdata")
fun bindtheRecycler(recyclerView: RecyclerView, data: List<Post>?) {
    val adapter = recyclerView.adapter as HomeRecyclerViewAdapter
    adapter.submitList(data)
    if (!data.isNullOrEmpty()) {
        recyclerView.visibility = View.VISIBLE
    }
}

@BindingAdapter("processVis")
fun visibilityOfProgress(progress: ProgressBar, data: List<Post>?) {
    if (!data.isNullOrEmpty()) {
        progress.visibility = View.GONE
    }
}
