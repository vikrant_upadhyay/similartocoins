package com.practice.kotlin_retrofit_proj.network

import com.practice.kotlin_retrofit_proj.HomeConstants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

object RetroFitBuilder {
    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(HomeConstants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    val api:RetroFitApiService by lazy {
        retrofit.create(RetroFitApiService::class.java)
    }
}