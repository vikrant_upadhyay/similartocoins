package com.practice.kotlin_retrofit_proj.network

import com.practice.kotlin_retrofit_proj.models.Post
import retrofit2.http.GET
import retrofit2.http.Path

interface RetroFitApiService {

    @GET(value = "todos/")
    suspend fun getAllPosts(): List<Post>

    @GET(value = "todos/{id}")
    suspend fun getDataById(@Path("id") id: Int): Post
}